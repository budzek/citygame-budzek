package com.example.android_7gps;

public class GpsPoint {

	public String Name, Image,Question,Answer1,Answer2,Answer3;
	public double Longitude, Latitude;
	int Rightanswer;

	public GpsPoint(String Name, String Image, double Longitude,
			double Latitude, String Question, String Answer1, 
			String Answer2, String Answer3, int Rightanswer) {
		this.Name = Name;
		this.Image = Image;
		this.Longitude = Longitude;
		this.Latitude = Latitude;
		this.Question=Question;
		this.Answer1=Answer1;
		this.Answer2=Answer2;
		this.Answer3=Answer3;
		this.Rightanswer=Rightanswer;
	}

}
