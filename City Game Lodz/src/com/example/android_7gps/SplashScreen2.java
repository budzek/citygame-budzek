package com.example.android_7gps;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Display;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SplashScreen2 extends Activity {

	ImageView image;
	int imageView_X, scrResolutionX, difference;
	Display display;
	Point point;
	Intent mainIntent,secondIntent;
	LinearLayout linearLayout;
	TextView title_tv,signification_tv;
	AlphaAnimation anim;
	Typeface typeFace1,typeFace2;

	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.splash_screen);

		// tworzenie okna gry
		mainIntent = new Intent(this, MainActivity.class);
		secondIntent= new Intent(this,ConfigScreen.class);
		bindViewVariables();
		setCustomFonts();
		
	
		
		runAnim();

	}

	public void runAnim() {
		// pobieranie inf o rozdzielczosci ekranu
		display = getWindowManager().getDefaultDisplay();
		point = new Point();
		display.getSize(point);
		scrResolutionX = point.x;
		
		// pobieranie inf o polozeniu obrazka
		imageView_X = (int) image.getX();

		// obliczanie roznicy w polozeniu obrazka a srodkiem ekranu
		difference = Math.abs(imageView_X - scrResolutionX);
		image.scrollBy((scrResolutionX / 2) - difference, 0);

		//animacja naglowka
		anim=new AlphaAnimation(0.0f, 1f);
		anim.setDuration(1500);
		anim.setFillAfter(true);
		anim.setAnimationListener(new AnimationListener() 
		{
			public void onAnimationStart(Animation animation) {}
			public void onAnimationRepeat(Animation animation) {}
			public void onAnimationEnd(Animation animation) 
			{	

				//wlaczanie kolejnego okienka
				//startActivity(mainIntent);
				startActivity(secondIntent);
				finish();
			}
		});
		
		//uruchamianie watku tworzacego animacje
		Runnable imageMove = new Runnable() {

			
			
			public void run() 
				{

				while (difference > 0) 
				{
					image.scrollBy(1, 0);
					try 
					{
						Thread.sleep(3);
					} 
					catch (InterruptedException e) { e.printStackTrace();}
					difference--;
				}

				title_tv.setAnimation(anim);	
				
			}
		};
		Thread thread = new Thread(imageMove);
		thread.start();
	}

	void bindViewVariables(){
		
		linearLayout = (LinearLayout) findViewById(R.id.Linear_layout_splashScreen);
		linearLayout.setBackgroundColor(Color.BLACK);
		image = (ImageView) findViewById(R.id.question_imageView);
		image.setImageResource(R.drawable.lodz_panorama);
		
		// TextView's
		title_tv = (TextView) findViewById(R.id.Title__splashScreen_tv);
		title_tv.setVisibility(TextView.INVISIBLE);
		signification_tv = (TextView) findViewById(R.id.signification_tv);
		
	}
	
	void setCustomFonts(){
		typeFace1 = Typeface.createFromAsset(getAssets(),"fonts/font_handwrite.ttf");
		signification_tv.setTypeface(typeFace1);
		
		typeFace2 = Typeface.createFromAsset(getAssets(), "fonts/font2.ttf");
		title_tv.setTypeface(typeFace2);
	}
	
	
}











