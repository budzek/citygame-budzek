package com.example.android_7gps;

import java.util.Date;

import android.location.Location;

public class User {
	String userName;
	Integer userId;
	Integer clanId;
	Location location;
	Date lastActivityTime;
	boolean active;
}
