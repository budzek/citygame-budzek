package com.example.android_7gps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class FinalResults extends ListActivity {

	private ProgressDialog pDialog;
	JSONParser jParser = new JSONParser();
	ArrayList<HashMap<String, String>> recordsList;
	ArrayList<String3>tempList;

	// link do skryptu php odczytujacego wszystkie rekordy w bazie
	private static String url_all_records = "http://inzynierka.site90.com/get_all_records.php";
;
	String path=null;
	JSONArray products = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.final_results);
		
		this.path=SV.path;
		Toast.makeText(FinalResults.this,"Tap list to exit game",Toast.LENGTH_LONG).show();
		recordsList = new ArrayList<HashMap<String, String>>();
		tempList = new ArrayList<String3>();

		// wczytywanie danych z bazy w tle aplikacji
		new GetAllRecords().execute();

		ListView lv = getListView();

		// zamknij okno gdy item listView jest klikniety
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				finish();
				
			}


		});

	}



	//klasa asynchroniczna do wczytywania danych z bazy w tle aplikacji
	class GetAllRecords extends AsyncTask<String, String, String> {


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(FinalResults.this);
			pDialog.setMessage("Loading Score list. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
			//pDialog.setTitle("XXXXX");
			pDialog.show();
		}

		//wykonuje dzialania w tle
		protected String doInBackground(String... args) {
			
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			JSONObject json = jParser.HttpRequest(url_all_records, "GET", params);
			


			try {
				
				int success = json.getInt("success");

				if (success == 1) {
					
					products = json.getJSONArray("score_list");

					
					for (int i = 0; i < products.length(); i++) 
					{
						JSONObject c = products.getJSONObject(i);
						String price = c.getString("score");
						String name = c.getString("nickname");
						String pid = c.getString("path");

						tempList.add(new String3(name,pid,price));
					}
					
					
					
					
					for (int i=0;i<tempList.size();i++){
						
						//SELEKCJA WYSWIETLANIA WYNIKOW POD WZGLEDEM SCIEZKI
						if(tempList.get(i).descr.equals(path))
						{
						HashMap<String, String> map = new HashMap<String, String>();
							map.put("score", tempList.get(i).price);
							map.put("nickname", tempList.get(i).name);
							recordsList.add(map);
						}
					}
					
					
				} 
				
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		//po zakonczonej pracy w tle, ladowanie danych do listView
		protected void onPostExecute(String file_url) {

			pDialog.dismiss();
			runOnUiThread(new Runnable() {
				public void run() {
					
					ListAdapter adapter = new SimpleAdapter(
							FinalResults.this, recordsList,
							R.layout.list_item, 
							new String[] {"nickname","score"},
							new int[] { R.id.pid, R.id.name });
					setListAdapter(adapter);
				}
			});

		}

	}
}