package com.example.android_7gps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class MainActivity extends Activity {
	/*
	 * - add user login - on login check on rest - add function to check ->
	 * return boolean 
	 * - load flags on startup from rest 
	 * 	- on each timer action
	 * (5 seconds ?) 
	 * - check if current location in proper distance to any of
	 * flags - if so post to flag - update flag list + on map - update user list
	 * + on map
	 */
	// <PBL variables>
	// List<Location> flagsList = new ArrayList<Location>();
	// List<Location> userList = new ArrayList<Location>();
	static {
		SV.path = "default_path";

		// SharedVariables
	}
	// </PBL variables>

	TextView czas_tv, extra_score_tv, achievements_tv;
	CheckBox mockLoc_ChckBx, places_ChckBx;
	Button mapType_btn;
	GoogleMap googleMap;
	int mapType_int = 2, stop_time = -1;
	TextToSpeech myTTS;
	ProgressBar progresBar;

	Dialog endDialog;
	boolean endDialogON = false;

	ArrayList<GpsPoint> PlacesList;
	// licznik
	Handler handler;
	Timer timer;
	Runnable timer_runnable;
	// listenery
	OnInitListener TTS_listener;
	OnMapClickListener onMapClick_listener;
	OnClickListener onClicklistener_googleMap;
	// OnCheckedChangeListener ckckBox_listener_mokLock,
	// ckckBox_listener_places;
	OnMyLocationChangeListener location_changed_listener;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_main);

		int resID = getResources().getIdentifier(SV.path,
				"drawable", getPackageName());
		PlacesList = new ArrayList<GpsPoint>();

		// new GetFlagsTask().execute("");
		// RestHelper.postForFlag(1);
		initMap();
		bindViewVariables();
		setVariables();
		runListeners();

		// Typ Mapy
		mapType_btn.setOnClickListener(onClicklistener_googleMap);
		// dodawanie markerow palcem
		googleMap.setOnMapClickListener(onMapClick_listener);
		// reakcja na zmiane pozycji GPS
		googleMap.setOnMyLocationChangeListener(location_changed_listener);
		// inicjalizowanie text to speech
		myTTS = new TextToSpeech(getBaseContext(), TTS_listener);

		runClock();

	}

	public void runClock() {
		czas_tv.setTextColor(Color.RED);
		handler = new Handler();
		timer_runnable = new Runnable() {
			byte setneSek = 0, sekundy = 0;
			byte minuty = 0, godziny = 0;
			String godziny_txt, minuty_txt, sekundy_txt;

			public void run() {
				if (stop_time != 1) {
					setneSek++;
				}

				if (setneSek >= 100) {
					setneSek = 0;
					sekundy++;
				}
				if (sekundy >= 60) {
					sekundy = 0;
					minuty++;
				}
				if (minuty >= 60) {
					minuty = 0;
					godziny++;
				}
				if (godziny < 10) {
					godziny_txt = "0" + String.valueOf(godziny);
				} else {
					godziny_txt = String.valueOf(godziny);
				}
				if (minuty < 10) {
					minuty_txt = "0" + String.valueOf(minuty);
				} else {
					minuty_txt = String.valueOf(minuty);
				}
				if (sekundy < 10) {
					sekundy_txt = "0" + String.valueOf(sekundy);
				} else {
					sekundy_txt = String.valueOf(sekundy);
				}

				SV.time_hours = godziny;
				SV.time_minutes = minuty;
				SV.time_seconds = sekundy;

				czas_tv.setText(godziny_txt + ":" + minuty_txt + ":"
						+ sekundy_txt);

			}
		};

		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				handler.post(timer_runnable);
				// TODO add here
				/*
				 * post current location get user locations
				 * 
				 * check if is in distance of any flag, if so post to flag
				 */
				refreshMap();
			}
		}, 1000, 10);

	}

	public void setVariables() {
		SV.total_places_number = PlacesList.size();
		progresBar.setMax(SV.total_places_number);
		// Log.d("komunikat", "koljeka : "+PlacesList.size(), null);

		progresBar.getProgressDrawable().setColorFilter(0xFF00FF00,
				Mode.MULTIPLY);
		mapType_btn.getBackground().setColorFilter(0xFF00FF00, Mode.MULTIPLY);

		achievements_tv.setText("Achievements: "
				+ SV.visited_places + "/"
				+ SV.total_places_number);
		extra_score_tv.setText("Extra score: " + SV.extra_points);
		
		SV.plClanMarkerIcon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
		SV.ulClanMarkerIcon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);

	}

	void bindViewVariables() {

		achievements_tv = (TextView) findViewById(R.id.achievements_tv);
		extra_score_tv = (TextView) findViewById(R.id.extra_score_tv);
		czas_tv = (TextView) findViewById(R.id.czas_tv);
		mapType_btn = (Button) findViewById(R.id.mapType_btn);
		progresBar = (ProgressBar) findViewById(R.id.Progresbar);

	}

	void runListeners() {

		onMapClick_listener = new OnMapClickListener() {

			public void onMapClick(LatLng arg0) {
				// dodawanie markera na mape
				// googleMap.addMarker(new MarkerOptions().position(arg0));
				// Log.d("komunikat", "Dodano marker - tap; long: "
				// + arg0.longitude + " lat: " + arg0.latitude);

				// dodwanie markera(miejsca) do kolejki
				// SharedVariables.PlacesList.add(new
				// GpsPoint("custom place","politechnika",arg0.longitude,arg0.latitude,"jakies tam dobre pytanie","odpowiedz1","odpowiedz2","odpowiedz3",2));

				if (PlacesList.size() > 0) {
					Random generator = new Random();
					int number = generator.nextInt(PlacesList.size());
					GpsPoint tempPoint = PlacesList.get(number);
					tempPoint.Latitude = arg0.latitude;
					tempPoint.Longitude = arg0.longitude;
					PlacesList.remove(number);
					PlacesList.add(tempPoint);
				}
				refreshMap();
				// Log.d("komunikat",
				// "ilosc elmentow liscie: "+PlacesList.size());

			}
		};

		onClicklistener_googleMap = new OnClickListener() {

			public void onClick(View v) {
				mapType_int++;
				if (mapType_int >= 5) {
					mapType_int = 0;
				}

				switch (mapType_int) {
				case 0: {
					googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
					Toast.makeText(getBaseContext(), "MAP TYPE HYBRID ",
							Toast.LENGTH_SHORT).show();
					break;
				}
				case 1: {
					googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
					Toast.makeText(getBaseContext(), "MAP TYPE NONE ",
							Toast.LENGTH_SHORT).show();
					break;
				}
				case 2: {
					googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					Toast.makeText(getBaseContext(), "MAP TYPE NORMAL ",
							Toast.LENGTH_SHORT).show();
					break;
				}
				case 3: {
					googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
					Toast.makeText(getBaseContext(), "MAP TYPE SATELLITE ",
							Toast.LENGTH_SHORT).show();
					break;
				}
				case 4: {
					googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
					Toast.makeText(getBaseContext(), "MAP TYPE TERRAIN ",
							Toast.LENGTH_SHORT).show();
					break;
				}
				default:
					break;
				}
			}
		};

		TTS_listener = new OnInitListener() {

			public void onInit(int status) {

				if (status == TextToSpeech.SUCCESS) {
					myTTS.setLanguage(Locale.UK);
					myTTS.speak(
							"Your task is to visit all places marked with markers as quick as it possible",
							TextToSpeech.QUEUE_FLUSH, null);
					// myTTS.playSilence(3000, TextToSpeech.QUEUE_ADD, null);
					// myTTS.speak("Type your nickname to start game",TextToSpeech.QUEUE_ADD,
					// null);
					Log.d("komunikat", "TextToSpeech works");
					// Log.d("komunikat", "Locale.getDefault() : "
					// + Locale.getDefault().toString());
				} else {
					if (status == TextToSpeech.ERROR) {
						Log.d("komunikat", "TextToSpeech error");
					}
				}

			}
		};

		location_changed_listener = new OnMyLocationChangeListener() {

			@Override
			public void onMyLocationChange(Location newLocation) {

				Location checkLocation = new Location("nie ma providera xD");
				SV.currentLocation = newLocation;

				if (PlacesList.size() > 0) {

					for (int i = 0; i < PlacesList.size(); i++) {

						checkLocation.setLongitude(PlacesList.get(i).Longitude);
						checkLocation.setLatitude(PlacesList.get(i).Latitude);

						if (newLocation.distanceTo(checkLocation) < 15) {

							// Toast.makeText(getBaseContext(),
							// "distance: "+newLocation.distanceTo(checkLocation)+" m",
							// Toast.LENGTH_LONG).show();
							// Log.d("komunikat",
							// "onMyLocationChange,dist: "+newLocation.distanceTo(checkLocation));

							final Dialog dialog = new Dialog(MainActivity.this);
							dialog.setContentView(R.layout.quiz_layout);
							dialog.setTitle(PlacesList.get(i).Name);
							TextView question_tv = (TextView) dialog
									.findViewById(R.id.question_tv);
							ImageView question_imageView = (ImageView) dialog
									.findViewById(R.id.question_imageView);
							final RadioGroup radioGroup = (RadioGroup) dialog
									.findViewById(R.id.question_radioGroup);
							final RadioButton radioButton1 = (RadioButton) dialog
									.findViewById(R.id.question_radio1);
							final RadioButton radioButton2 = (RadioButton) dialog
									.findViewById(R.id.question_radio2);
							final RadioButton radioButton3 = (RadioButton) dialog
									.findViewById(R.id.question_radio3);
							// Button questionButton =
							// (Button)dialog.findViewById(R.id.question_button);
							int resID = getResources().getIdentifier(
									PlacesList.get(i).Image, "drawable",
									getPackageName());
							question_imageView.setImageResource(resID);
							question_tv.setText(PlacesList.get(i).Question);
							radioButton1.setText(PlacesList.get(i).Answer1);
							radioButton2.setText(PlacesList.get(i).Answer2);
							radioButton3.setText(PlacesList.get(i).Answer3);
							final int RightAnswer = PlacesList.get(i).Rightanswer;

							radioGroup
									.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

										@Override
										public void onCheckedChanged(
												RadioGroup group, int checkedId) {

											switch (checkedId) {
											case R.id.question_radio1: {
												if (RightAnswer == 1) {
													Toast.makeText(
															getBaseContext(),
															"GOOD answer!\nExtra points for You :)",
															Toast.LENGTH_SHORT)
															.show();
													myTTS.speak(
															"Good Answer! Extra points for you!",
															TextToSpeech.QUEUE_FLUSH,
															null);
													SV.extra_points += 100;
												} else {
													Toast.makeText(
															getBaseContext(),
															"WRONG answer!",
															Toast.LENGTH_LONG)
															.show();
													myTTS.speak(
															"Wrong Answer!",
															TextToSpeech.QUEUE_FLUSH,
															null);
												}
												refreshMap();
												dialog.dismiss();
												break;
											}
											case R.id.question_radio2: {
												if (RightAnswer == 2) {
													Toast.makeText(
															getBaseContext(),
															"GOOD answer!\nExtra points for You :)",
															Toast.LENGTH_SHORT)
															.show();
													myTTS.speak(
															"Good Answer! Extra points for you!",
															TextToSpeech.QUEUE_FLUSH,
															null);
													SV.extra_points += 100;
												} else {
													Toast.makeText(
															getBaseContext(),
															"WRONG answer!",
															Toast.LENGTH_LONG)
															.show();
													myTTS.speak(
															"Wrong Answer!",
															TextToSpeech.QUEUE_FLUSH,
															null);
												}
												refreshMap();
												dialog.dismiss();
												break;
											}
											case R.id.question_radio3: {
												if (RightAnswer == 3) {
													Toast.makeText(
															getBaseContext(),
															"GOOD answer!\nExtra points for You :)",
															Toast.LENGTH_SHORT)
															.show();
													myTTS.speak(
															"Good Answer! Extra points for you!",
															TextToSpeech.QUEUE_FLUSH,
															null);
													SV.extra_points += 100;
												} else {
													Toast.makeText(
															getBaseContext(),
															"WRONG answer!",
															Toast.LENGTH_LONG)
															.show();
													myTTS.speak(
															"Wrong Answer!",
															TextToSpeech.QUEUE_FLUSH,
															null);
												}
												refreshMap();
												dialog.dismiss();
												break;
											}

											default: {
												break;
											}
											}

											// progres gry ++
											SV.visited_places++;
											achievements_tv
													.setText("Achievements: "
															+ SV.visited_places
															+ "/"
															+ SV.total_places_number);
											extra_score_tv
													.setText("Extra score: "
															+ SV.extra_points);
											progresBar
													.setProgress(SV.visited_places);

											if (SV.visited_places == SV.total_places_number) {
												endGame();
											}
										}

									});

							dialog.show();
							myTTS.speak(
									"Answer the question to get extra points.",
									TextToSpeech.QUEUE_FLUSH, null);
							myTTS.playSilence(1000, TextToSpeech.QUEUE_ADD,
									null);
							myTTS.speak(PlacesList.get(i).Question,
									TextToSpeech.QUEUE_ADD, null);

							// usuwanie odwiedzonego punktu z mapy
							PlacesList.remove(i);

						}

					}

				}

				// if(SharedVariables.visited_places==SharedVariables.total_places_number){
				// endGame();
				// }

			}
		};

	}

	void endGame() {
		stop_time = 1;
		String name = SV.login;
		SV.total_points = (5 * 3600 - SV.time_hours
				* 3600 + SV.time_minutes * 60 + SV.time_seconds)
				/ 10 + SV.extra_points;

		myTTS.speak("Congratulations " + name + ", all places visited !",
				TextToSpeech.QUEUE_FLUSH, null);
		myTTS.playSilence(1000, TextToSpeech.QUEUE_ADD, null);
		myTTS.speak(
				"You have just reached the end of City Game. Your total Score is "
						+ SV.total_points + "points",
				TextToSpeech.QUEUE_ADD, null);

		int hours = SV.time_hours;
		int minutes = SV.time_minutes;
		int seconds = SV.time_seconds;
		int extra_points = SV.extra_points;

		googleMap.setMyLocationEnabled(false);

		Builder endDialogBuilder = new AlertDialog.Builder(MainActivity.this)
				.setTitle("Congratulations " + name + ", all places visited !")
				.setMessage(
						"\nYou have just reached the end of City Game !\n\nYour time is:  "
								+ hours + ":" + minutes + ":" + seconds
								+ "\nExtra points:  " + extra_points
								+ "\nTotal points:  "
								+ SV.total_points);
		endDialog = endDialogBuilder.create();
		// endDialog.
		endDialog.show();
		endDialog.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface dialog) {

				new SaveResults(MainActivity.this).execute();

				Intent i = new Intent(MainActivity.this, FinalResults.class);
				startActivity(i);
				// finish();
			}
		});

	}

	void initMap() {
		try {
			if (googleMap == null) {
				googleMap = ((MapFragment) getFragmentManager()
						.findFragmentById(R.id.MapFragment)).getMap();
				googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
				googleMap.getUiSettings().setCompassEnabled(true);
				googleMap.setMyLocationEnabled(true);
				googleMap.getUiSettings().setMyLocationButtonEnabled(true);
				parseXML();
				/**************************************/
				loadFromRest();
				/**************************************/

				
				refreshMap();

				googleMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {

					@Override
					public void onMapLoaded() {
						if (googleMap.getMyLocation() != null) {
							LatLng myPosition = new LatLng(googleMap
									.getMyLocation().getLatitude(), googleMap
									.getMyLocation().getLongitude());
							CameraPosition cameraPosition = new CameraPosition.Builder()
									.zoom(19).tilt(67.5f).target(myPosition)
									.build();
							CameraUpdate zoom = CameraUpdateFactory
									.newCameraPosition(cameraPosition);
							googleMap.animateCamera(zoom, 5000, null);
						}

					}
				});
			}
		} catch (Exception e) {
		}
	}

	void refreshMap() {
		googleMap.clear();

		if (SV.flagsList != null && SV.flagsList.size() > 0) {
			for(Flag flag : SV.flagsList){
				googleMap.addMarker(
					new MarkerOptions()
					.position(new LatLng(flag.location.getLatitude(), flag.location.getLongitude()))
					.title("Flag : " + flag.flagId)
					.icon(flag.clanId == SV.ulClanId ? SV.ulClanMarkerIcon :  SV.plClanMarkerIcon) );
			}
		}
		
		if (SV.userList != null && SV.userList.size() > 0) {
			for(User user : SV.userList){
				//TODO uncomment
//				if(user.active){
					googleMap.addMarker(
						new MarkerOptions()
						.position(new LatLng(user.location.getLatitude(), user.location.getLongitude()))
						.title("User : " + user.userId)
						.icon(user.clanId == SV.ulClanId ? SV.ulClanMarkerIcon :  SV.plClanMarkerIcon) );
//				}
				//TODO ikony user�w mniejsze ? inne ni� flagi
				//TODO user name zamiast id 
				//TODO 
			}
		}
		
		RestHelper.postForLocation(SV.currentLocation.getLatitude(), SV.currentLocation.getLongitude());
		checkFlags();

	}

	public void readXmlFile() {

		int resID = getResources().getIdentifier(SV.path, "raw",
				getPackageName());

		InputStreamReader is = new InputStreamReader(this.getResources()
				.openRawResource(resID));
		BufferedReader bfReader = new BufferedReader(is);
		StringBuilder text = new StringBuilder();
		String line;
		try {
			while ((line = bfReader.readLine()) != null) {
				text.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void parseXML() throws IOException, XmlPullParserException {

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();

		int resID = getResources().getIdentifier(SV.path, "raw",
				getPackageName());

		xpp.setInput(this.getResources().openRawResource(resID), null);

		xpp.nextTag();
		xpp.require(XmlPullParser.START_TAG, null, "root");
		while (xpp.nextTag() == XmlPullParser.START_TAG) {

			xpp.require(XmlPullParser.START_TAG, null, "point");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "name");
			String Name = xpp.nextText();
			xpp.require(XmlPullParser.END_TAG, null, "name");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "image");
			String Image = xpp.nextText();
			xpp.require(XmlPullParser.END_TAG, null, "image");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "long");
			double longi = Double.valueOf(xpp.nextText());
			xpp.require(XmlPullParser.END_TAG, null, "long");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "lat");
			double lati = Double.valueOf(xpp.nextText());
			xpp.require(XmlPullParser.END_TAG, null, "lat");

			// ---------------------
			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "question");
			String Question = xpp.nextText();
			xpp.require(XmlPullParser.END_TAG, null, "question");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "answer1");
			String Answer1 = xpp.nextText();
			xpp.require(XmlPullParser.END_TAG, null, "answer1");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "answer2");
			String Answer2 = xpp.nextText();
			xpp.require(XmlPullParser.END_TAG, null, "answer2");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "answer3");
			String Answer3 = xpp.nextText();
			xpp.require(XmlPullParser.END_TAG, null, "answer3");

			xpp.nextTag();
			xpp.require(XmlPullParser.START_TAG, null, "rightanswer");
			int Rightanswer = Integer.parseInt(xpp.nextText());
			xpp.require(XmlPullParser.END_TAG, null, "rightanswer");

			// ---------------------

			xpp.nextTag();
			xpp.require(XmlPullParser.END_TAG, null, "point");

			PlacesList.add(new GpsPoint(Name, Image, longi, lati, Question,
					Answer1, Answer2, Answer3, Rightanswer));

		}
		xpp.require(XmlPullParser.END_TAG, null, "root");

	}

	// TTS - destroy
	protected void onDestroy() {
		if (myTTS != null) {
			myTTS.stop();
			myTTS.shutdown();
			Log.d("komunikat", "Text to speech - onDestroy");
		}
		super.onDestroy();
	}

	// wcisniecie klawisza "wstecz"

	public void onBackPressed() {
		// super.onBackPressed();
		myTTS.speak("Are you sure you want to exit the City Game",
				TextToSpeech.QUEUE_FLUSH, null);

		new AlertDialog.Builder(this)
				.setTitle("EXIT")
				.setMessage(
						"Are you sure you want to \nexit the City Game ��d�?")
				.setNegativeButton("No", null)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {
								// finish();
								System.exit(1);
							}
						}).show();
	}

	public void checkFlags() {
		for (Flag flag : SV.flagsList) {
			if (flag.location.distanceTo(SV.currentLocation) < flag.takeOutRadius) {
				RestHelper.postForFlag(flag.flagId);
			}
		}
	}
	
	public void loadFromRest(){
		new GetFlagsTask().execute("");
		new GetUsersTask().execute("");
	}

}
