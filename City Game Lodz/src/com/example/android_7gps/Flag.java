package com.example.android_7gps;

import android.location.Location;

public class Flag {
	public int flagId;
	public int clanId;
	public Location location;
	public double takeOutRadius;
	public boolean isTakenOut;
	public int clanTime;
	public int takeOutTime;
}
