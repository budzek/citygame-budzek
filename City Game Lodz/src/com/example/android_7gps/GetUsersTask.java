package com.example.android_7gps;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.os.AsyncTask;

public class GetUsersTask extends AsyncTask<String, Void, String> {

	@Override
	protected String doInBackground(String... arg0) {
		try {
			SV.userList = new ArrayList<User>();

			JSONParser jParser = new JSONParser();
			JSONArray json = jParser.getJSON(RestHelper.getLocationsJSON());
			JSONObject jsonObject;
			double lat, lng; 
			Long time;
			User user;
			for (int i = 0; i < json.length(); i++) {
				user = new User();
				jsonObject = json.getJSONObject(i);

				lat = jsonObject.getDouble("lat");
				lng = jsonObject.getDouble("lng");
				user.location = new Location("rest service provider");
				user.location.setLatitude(lat);
				user.location.setLongitude(lng);
				//TODO change to user name
				user.userName = "User: "+jsonObject.getInt("userId");
				user.userId = jsonObject.getInt("userId");
				//TODO get user clan
				//user.clanId = jsonObject.getInt("clanId");
				
				//TODO 
				/*
				 * to por�wnanie + to czy user jest aktywny MUSI by� po stronie servera.
				 * Ewentualnie funkcja do pobierania czasu servera
				 */
				
				time = jsonObject.getLong("time");
				user.lastActivityTime = new Date(time);
				
				//if inactive for 30 seconds mark inactive
				if((new Date()).getTime() > user.lastActivityTime.getTime() + SV.idleTime )
					user.active = false;
				else
					user.active = true;
				
				SV.userList.add(user); 
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

}
