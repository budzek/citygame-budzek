package com.example.android_7gps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

public class RestHelper {
//TODO setting user current location on each timer beep
	public static final String getLocationsURL = "http://citygame-budzek.rhcloud.com/rest/locations/get";
	public static final String getFlagsURL = "http://citygame-budzek.rhcloud.com/rest/locations/flags/get";
	public static final String postForFlagURL = "http://citygame-budzek.rhcloud.com/rest/user/location/set/";
	public static final String postForLocationURL = "http://citygame-budzek.rhcloud.com/rest/user/location/set/";


	public static String getLocationsJSON() {
		String resp = GET(getLocationsURL);
		Log.d("MyDebug", resp);
		return resp;
	}
	
	public static String getFlagsJSON() {
		String resp = GET(getFlagsURL);
		Log.d("MyDebug", resp);
		return resp;
	}
	
	public static void postForFlag(final int flagId){
		AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>(){
			@Override
			protected String doInBackground(String... params) {
				String s = GET(postForFlagURL + flagId);
				return s;
			}
		};
		task.execute();
	}
	
	public static void postForLocation(final double lat, final double lng){
		AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>(){
			@Override
			protected String doInBackground(String... params) {
				String s = GET(postForLocationURL + lat + "/" + lng);

				return s;
			}
		};
		task.execute();
	}

	public static String GET(String url) {
		InputStream inputStream = null;
		String result = "";
		try {
			// create HttpClient
			HttpClient  httpClient = new DefaultHttpClient();
			// make GET request to the given URL
			HttpGet httpGet = new HttpGet(url);

			// pluser/pluser
			// Authorization:Basic cGx1c2VyOnBsdXNlcg==
			//httpGet.setHeader("Authorization", "Basic cGx1c2VyOnBsdXNlcg==");
			String authHeader = getAuthHeader(SV.login + ":" + SV.password);
			httpGet.setHeader("Authorization", authHeader);
			HttpResponse httpResponse = httpClient.execute(httpGet);

			// receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// convert inputstream to string
			if (inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";

		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}

		return result;
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}
	
//	public static List<Flag> getFlagList(){
//		return null;
//	}
	

	
	private static String getAuthHeader(String text){
	    byte[] data = null;
	    try {
	        data = text.getBytes("UTF-8");
	    } catch (UnsupportedEncodingException e1) {
	    e1.printStackTrace();
	    }
	    String base64 = Base64.encodeToString(data, Base64.DEFAULT);
	    // it's done to remove \n at the end of string
	    base64 = base64.replace("\n", ""); 

		return "Basic " + base64;
	}
}
