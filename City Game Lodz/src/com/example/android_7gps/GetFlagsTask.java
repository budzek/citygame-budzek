package com.example.android_7gps;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

public class GetFlagsTask extends AsyncTask<String, Void, String> {

	@Override
	protected String doInBackground(String... arg0) {
		try {
			SV.flagsList = new ArrayList<Flag>();

			JSONParser jParser = new JSONParser();
			JSONArray json = jParser.getJSON(RestHelper.getFlagsJSON());
			JSONObject jsonObject;
			double lat, lng; 
			Flag flag;
			for (int i = 0; i < json.length(); i++) {
				flag = new Flag();
				jsonObject = json.getJSONObject(i);
				flag.clanId = jsonObject.getInt("clan");
				flag.flagId = jsonObject.getInt("id");
				lat = jsonObject.getDouble("lat");
				lng = jsonObject.getDouble("lng");
				flag.location = new Location("rest servie provider");
				flag.location.setLatitude(lat);
				flag.location.setLongitude(lng);
				flag.clanTime = jsonObject.getInt("clanTime");
				flag.takeOutTime = jsonObject.getInt("takeOutTime");
				flag.takeOutRadius = jsonObject.getDouble("takeOutRadius");
				flag.isTakenOut = jsonObject.getBoolean("isTakenOut");
				SV.flagsList.add(flag); 
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

}