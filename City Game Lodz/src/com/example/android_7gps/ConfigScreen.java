package com.example.android_7gps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


public class ConfigScreen extends Activity {
	Intent mainIntent;
	ListView listView;
	EditText imie_et, pass_et;
	ArrayAdapter<String> listAdapter ;
	TextToSpeech myTTS;
	OnInitListener TTS_listener;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.config_layout);
		
		//StrictMode.enableDefaults();
//		myTTS = new TextToSpeech(getBaseContext(), new OnInitListener() {
//			
//			public void onInit(int status) {
//
//				if (status == TextToSpeech.SUCCESS) {
//					myTTS.setLanguage(Locale.UK);
//					myTTS.speak("Welcome to City game - I will be your guide",TextToSpeech.QUEUE_FLUSH, null);
//					myTTS.playSilence(1000, TextToSpeech.QUEUE_ADD, null);
//					myTTS.speak("Type your nickname and choose path to play game",TextToSpeech.QUEUE_ADD, null);
//
//				} else {
//					if (status == TextToSpeech.ERROR) {
//						Log.d("komunikat", "TextToSpeech error");
//					}
//				}
//
//			}
//		});
		
		imie_et = (EditText) findViewById(R.id.editText1);
		pass_et = (EditText) findViewById(R.id.editText2);

		mainIntent = new Intent(this, MainActivity.class);

		final Button button = (Button) findViewById(R.id.loginButton);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (!imie_et.getText().toString().equals("")
						&& !pass_et.getText().toString().equals("")) {

					SV.login = imie_et.getText().toString();
					SV.password = pass_et.getText().toString();
					//TODO try some method to see if pass / login is ok
					/*
					 * try some method to see if pass / login is ok
					 * 	- if not toast
					 */
					
					startActivity(mainIntent);
					finish();

				} else {
					Toast.makeText(getApplicationContext(),
							"Type your nickname and password",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	
	// TTS - destroy
	protected void onDestroy() {
//		if (myTTS != null) {
//			myTTS.stop();
//			myTTS.shutdown();
//			//Log.d("komunikat", "Text to speech - onDestroy");
//		}
		super.onDestroy();
	}
	
	
}
