package com.example.android_7gps;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;


class SaveResults extends AsyncTask<String, String, String> {

	private ProgressDialog pDialog;
	Context context;
	JSONParser jsonParser = new JSONParser();
	String url_create_product = "http://inzynierka.site90.com/create_record.php";

	
	protected void onPreExecute() 
	{
		super.onPreExecute();
		pDialog = new ProgressDialog(context);
		pDialog.setMessage("Adding Your score to database...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);
		pDialog.show();

	}
	public SaveResults(Context context){
		this.context=context;
	}


	protected String doInBackground(String... args) {

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		params.add(new BasicNameValuePair("nickname", SV.login));
		params.add(new BasicNameValuePair("score", String.valueOf(SV.total_points)));
		params.add(new BasicNameValuePair("path", SV.path));
		//przesylanie recordu do bazy za pomoca klasy JSONParser
		JSONObject json = jsonParser.HttpRequest(url_create_product,"POST", params);

		return null;
	}


	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
	}


	private void startActivity(Intent i) {
		// TODO Auto-generated method stub
		
	}

}