package com.example.android_7gps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 
public class JSONParser {
 
    InputStream inputStream;
    JSONObject jsonObject;
    String result = "";
    JSONArray jsonArray;
 

 
    
    public JSONObject HttpRequest(String url, String actionType,
            List<NameValuePair> params) {
 
        
        try {
 
            // zapisywanie rekord�w w bazie
            if(actionType == "POST")
            {
                
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params));
 
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                inputStream = httpEntity.getContent();
            }
            
            // odczytywanie rekord�w
            else if(actionType == "GET")
            {
                
                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);
 
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                inputStream = httpEntity.getContent();
            }           
 
            
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        try {
            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedreader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            inputStream.close();
            result = stringBuilder.toString();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
 
        //parsowanie String do JSONobject
        try {
            jsonObject = new JSONObject(result);
        } catch (JSONException e) {
            System.out.println(e.toString());
        }
 
        return jsonObject;
 
    }
    
//    public JSONArray getJSON(InputStream inputStream){
//        try {
//            BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
//            StringBuilder stringBuilder = new StringBuilder();
//            String line;
//            while ((line = bufferedreader.readLine()) != null) {
//                stringBuilder.append(line + "\n");
//            }
//            inputStream.close();
//            result = stringBuilder.toString();
//        } catch (Exception e) {
//            System.out.println(e.toString());
//        }
// 
//        //parsowanie String do JSONobject
//        try {
////            jsonObject = new JSONObject(result);
//            
//            jsonArray = new JSONArray(result);
//        } catch (JSONException e) {
//            System.out.println(e.toString());
//        }
// 
//        return jsonArray;
//    }
    
    public JSONArray getJSON(String result){
        //parsowanie String do JSONobject
        try {
//            jsonObject = new JSONObject(result);
            jsonArray = new JSONArray(result);
        } catch (JSONException e) {
            System.out.println(e.toString());
        }
 
        return jsonArray;
    }
}